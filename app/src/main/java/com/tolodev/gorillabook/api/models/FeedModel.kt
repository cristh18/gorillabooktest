package com.tolodev.gorillabook.api.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FeedModel(
    @field:Json(name = "id") val id: Int,
    @field:Json(name = "first_name") val firstName: String,
    @field:Json(name = "last_name") val lastName: String,
    @field:Json(name = "post_body") val postBody: String,
    @field:Json(name = "unix_timestamp") val unixTimestamp: Long,
    @field:Json(name = "image") val image: String?
){
    fun getFullName() = firstName.plus(" ").plus(lastName)
}