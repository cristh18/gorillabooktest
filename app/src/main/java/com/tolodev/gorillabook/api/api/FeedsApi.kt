package com.tolodev.gorillabook.api.api

import com.tolodev.gorillabook.api.models.FeedModel
import retrofit2.http.GET

interface FeedsApi {

    @GET("feed")
    suspend fun getFeeds(): List<FeedModel>
}