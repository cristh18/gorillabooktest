package com.tolodev.gorillabook.ui.views

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.tolodev.gorillabook.R
import com.tolodev.gorillabook.api.models.FeedModel
import com.tolodev.gorillabook.databinding.ViewItemFeedBinding

class ItemFeedView(context: Context) : FrameLayout(context) {

    private val binding: ViewItemFeedBinding =
        DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_item_feed, this, true)

    lateinit var feedModel: FeedModel

    init {
        layoutParams = LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    fun bindView() {
        binding.feed = feedModel
        loadImage()
        binding.root.hasFocus()
    }

    private fun loadImage() {
        if (!feedModel.image.isNullOrBlank()) {
            Glide
                .with(context)
                .load(feedModel.image)
                .centerCrop()
                .placeholder(R.drawable.ic_gorilla_logo)
                .into(binding.imageViewPost)
        }
    }
}