package com.tolodev.gorillabook.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import com.tolodev.gorillabook.R
import com.tolodev.gorillabook.databinding.ActivityMainBinding
import com.tolodev.gorillabook.ui.adapters.FeedAdapter
import com.tolodev.gorillabook.ui.fragments.CreatePostFragment
import com.tolodev.gorillabook.ui.viewModels.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val viewModel by viewModel<MainViewModel>()

    private val feedAdapter: FeedAdapter = FeedAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        subscribe()
        setupView()
        initListeners()
    }

    private fun setupView() {
        binding.recyclerViewRates.apply {
            adapter = feedAdapter
            addItemDecoration(DividerItemDecoration(baseContext, DividerItemDecoration.VERTICAL))
        }
    }

    private fun subscribe() {
        viewModel.onShowFeeds().observe(this, Observer { feedAdapter.setItems(it) })
    }


    private fun initListeners() {
        binding.buttonCratePost.setOnClickListener {
            addFragment(CreatePostFragment(), binding.container.id, true)
        }
    }

    private fun addFragment(
        fragment: Fragment,
        containerViewId: Int,
        addToStack: Boolean
    ) {
        if (!fragment.isAdded) {
            val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
            ft.add(containerViewId, fragment, fragment.javaClass.name)
            if (addToStack) {
                ft.addToBackStack(fragment.javaClass.name)
            }
            ft.commit()
        }
    }
}
