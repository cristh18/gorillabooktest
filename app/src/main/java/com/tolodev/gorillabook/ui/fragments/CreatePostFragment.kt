package com.tolodev.gorillabook.ui.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil

import com.tolodev.gorillabook.R
import com.tolodev.gorillabook.databinding.FragmentCreatePostBinding

class CreatePostFragment : Fragment() {

    private lateinit var binding: FragmentCreatePostBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_create_post, container, false)
        setupView()
        initListeners()
        return binding.root
    }

    private fun setupView(){

    }

    private fun initListeners(){
        binding.buttonAddPhoto.setOnClickListener {

        }
    }

}
