package com.tolodev.gorillabook.ui.viewModels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tolodev.gorillabook.api.models.FeedModel
import com.tolodev.gorillabook.data.repository.FeedRepository
import kotlinx.coroutines.*

class MainViewModel(private val feedRepository: FeedRepository) : ViewModel() {

    private val viewModelJob = Job()

    private val viewmodelCoroutineScope = CoroutineScope(Dispatchers.IO + viewModelJob)

    private val showError = MutableLiveData<Throwable>()

    private val showFeeds = MutableLiveData<List<FeedModel>>()

    init {
        loadData()
    }

    private fun loadData() {
        GlobalScope.launch(Dispatchers.Main) {
            val coroutineExceptionHandler =
                CoroutineExceptionHandler { _, exception -> showError.postValue(exception) }

            viewmodelCoroutineScope.launch(coroutineExceptionHandler) {
                val feeds: List<FeedModel> = feedRepository.getFeeds()
                Log.e("POKEMON: ", "FEEDS: ".plus(feeds.toString()))
                showFeeds.postValue(feeds)
            }
        }
    }

    fun onShowFeeds(): LiveData<List<FeedModel>> = showFeeds
}