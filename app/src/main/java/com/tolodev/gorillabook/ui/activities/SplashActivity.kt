package com.tolodev.gorillabook.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import androidx.databinding.DataBindingUtil
import com.tolodev.gorillabook.R
import com.tolodev.gorillabook.databinding.ActivitySplashBinding

class SplashActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)
        initAnimation()
    }

    private fun initAnimation() {
        object : CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
            }
            override fun onFinish() {
                finishSplashAnimations()
            }
        }.start()

    }

    private fun finishSplashAnimations() {
        startActivity(Intent(baseContext, MainActivity::class.java))
    }
}
