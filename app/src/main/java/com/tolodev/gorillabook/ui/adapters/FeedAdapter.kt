package com.tolodev.gorillabook.ui.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tolodev.gorillabook.api.models.FeedModel
import com.tolodev.gorillabook.ui.views.ItemFeedView

class FeedAdapter :
    RecyclerView.Adapter<FeedAdapter.ItemFeedViewHolder>() {

    private val items = mutableListOf<FeedModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemFeedViewHolder {
        return ItemFeedViewHolder(ItemFeedView(parent.context))
    }

    override fun onBindViewHolder(holder: ItemFeedViewHolder, position: Int) {
        holder.itemFeedView.apply {
            feedModel = items[position]
            bindView()
        }
    }

    override fun getItemCount(): Int = items.size

    fun setItems(items: List<FeedModel>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    inner class ItemFeedViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val itemFeedView: ItemFeedView = view as ItemFeedView
    }
}