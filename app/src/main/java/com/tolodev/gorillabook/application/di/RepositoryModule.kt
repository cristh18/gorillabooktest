package com.tolodev.gorillabook.application.di

import com.tolodev.gorillabook.data.repository.FeedRepository
import org.koin.dsl.module

val repositoryModule = module(override = true) {
    single { FeedRepository(feedsApi = get()) }
}