package com.tolodev.gorillabook.application.di

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.tolodev.gorillabook.R
import com.tolodev.gorillabook.api.api.FeedsApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

private val REQUEST_TIMEOUT = 60L

val netModule = module(override = true) {

    single<Retrofit>(named("feeds")) {
        Retrofit.Builder()
            .client(get())
            .baseUrl(androidContext().getString(R.string.base_url))
            .addConverterFactory(MoshiConverterFactory.create(get()))
            .build()
    }

    fun provideOkHttpClient(): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
            .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        httpClient.addInterceptor(interceptor)

        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val requestBuilder = original.newBuilder()
                .addHeader("Accept", "application/json")
                .addHeader("Content-Type", "application/json")
            requestBuilder.method(chain.request().method, chain.request().body)

            val request = requestBuilder.build()
            chain.proceed(request)
        }
        return httpClient.build()
    }

    fun provideMoshiBuilder(): Moshi {
        return Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
    }

    single { provideOkHttpClient() }
    single { provideMoshiBuilder() }
    factory { get<Retrofit>(named("feeds")).create(FeedsApi::class.java) }
}