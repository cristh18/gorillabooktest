package com.tolodev.gorillabook.application

import android.app.Application
import com.tolodev.gorillabook.application.di.netModule
import com.tolodev.gorillabook.application.di.repositoryModule
import com.tolodev.gorillabook.application.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class GorillaBookApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@GorillaBookApplication)
            androidLogger(Level.DEBUG)
            modules(listOf(viewModelModule, repositoryModule, netModule))
        }
    }
}