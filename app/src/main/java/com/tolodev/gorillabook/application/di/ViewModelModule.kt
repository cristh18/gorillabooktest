package com.tolodev.gorillabook.application.di

import com.tolodev.gorillabook.ui.viewModels.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module(override = true) {
    viewModel { MainViewModel(get()) }
}