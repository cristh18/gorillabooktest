package com.tolodev.gorillabook.data.repository

import com.tolodev.gorillabook.api.api.FeedsApi
import com.tolodev.gorillabook.api.models.FeedModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

private val BASE_CURRENCY = "EUR"

class FeedRepository(
    private val feedsApi: FeedsApi
) {

    suspend fun getFeeds(): List<FeedModel> {

        val feedResponse =
            withContext(CoroutineScope(Dispatchers.IO).coroutineContext) {
                feedsApi.getFeeds()
            }

        return feedResponse.toList()
    }
}